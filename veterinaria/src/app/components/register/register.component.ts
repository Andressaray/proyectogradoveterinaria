import { Component, OnInit }  from '@angular/core';
import { Router }             from '@angular/router';

import Swal                   from 'sweetalert2';

import { AuthService }        from '../../services/service.service';
import { UserN }              from '../../models/user';
import { HeaderComponent }    from '../header/header.component';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit {
  public user: UserN;
  constructor(private authService: AuthService, private router: Router) {
    this.user = {
      id:       null,
      name:     '',
      lastname: '',
      phone:    null,
      email:    '',
      password: ''
    };
  }
  
  ngOnInit() {
    this.prevent();
  }

  prevent (): void{
    if(sessionStorage.getItem('id')){
      Swal.fire({
        icon: 'info',
        title: 'Ya tienes una sesión abierta',
        showConfirmButton: false,
        timer: 1500
      });
      this.router.navigateByUrl('/index');
    }
  }

  seePassword(event: Event): void{
    const password =  document.getElementById('password');
    if(password['type'] == "password"){
      password['type'] = "email";
    }else{
      password['type'] = "password";
    }
    event.preventDefault();
  }

  onRegister(): void {
    const reg = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    if(!reg.test(this.user.email)){
      Swal.fire({
        icon: 'info',
        title: 'Información',
        text: 'Tu correo no es valido',
        showConfirmButton: true
      })
      return
    }
    console.log(this.user);
      this.authService.register(this.user).subscribe(
        res => {
          Swal.fire({
            icon: 'success',
            title: 'Iniciaste sesion',
            text: `Bienvenido ${sessionStorage.getItem('name')}`,
            showConfirmButton: false,
            timer: 1500
          });
          this.router.navigateByUrl('/index');
      },
        error => {
          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: 'No pudiste registrarte',
            showConfirmButton: false,
            timer: 1500
          });
        });
  }
}
