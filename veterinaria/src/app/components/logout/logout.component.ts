import { Component, OnInit }  from '@angular/core';
import { Router }             from '@angular/router';

import Swal                   from 'sweetalert2';

import { AuthService }        from '../../services/service.service';

@Component({
  selector: 'app-logout',
  template: ''
})
export class LogoutComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
    if(sessionStorage.getItem('name')){
      const id :number = parseInt(sessionStorage.getItem('id'));
      this.authService.logout({id})
        .subscribe(
          (res) => {
            Swal.fire({
              icon: 'success',
              title: 'Has cerrado sesión con exito',
              showConfirmButton: false,
              timer: 1500
            });
          },
          (error) => {
            Swal.fire({
              icon: 'error',
              title: 'No se ha podido cerrar tu sesión',
              showConfirmButton: false,
              timer: 1500
            });
          }
        );
      this.router.navigateByUrl('/index');
    }
    else{
      this.router.navigateByUrl('/index');
    }
  }

}
