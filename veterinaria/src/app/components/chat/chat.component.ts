import { Component, OnInit }  from '@angular/core';
import { Router }             from '@angular/router';

import { AuthService }        from '../../services/service.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {
  name: string = sessionStorage.getItem('name');
  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
  }
}
