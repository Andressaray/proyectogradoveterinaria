import { Component, OnInit }  from '@angular/core';
import { Router }             from '@angular/router';

import { saveAs }             from 'file-saver'
import Swal                   from 'sweetalert2';
import { ThemePalette }       from '@angular/material/core';

import { AuthService }        from '../../services/service.service';
import { Pet }                from 'src/app/models/pet';
import { AppointmentI }       from 'src/app/models/appointment'


@Component({
  selector: 'app-appointment',
  templateUrl: './appointment.component.html',
  styleUrls: ['./appointment.component.css'],
})
export class AppointmentComponent implements OnInit {
  public appointmentsUsers:     any;
  public petsUserAppointments:  any;
  public appointment:           AppointmentI;
  public appointmentInAttend:   any;
  public appointmentsDoctor:    any;
  public appointmentAttend:     any;
  public activeAttend:          boolean = false;
  public color:                 ThemePalette = 'accent';
  loading:                      boolean = false;
  public typesAppointments:     string [] = ['General', 
                                            'Sala de Cirugia', 
                                            'Vacunación', 
                                            'Revisión Oftalmológica',
                                            'Baño y peluqueria',
                                            'Estrelizacion para caninos y felinos'
                                          ];
  petAttendDoctor:              any;
  petResultQuery:               any;
  foundPets:                    boolean = true;
  userA:                        boolean = false;
  petAttend:                    any;
  url:                          string = this.authService.AUTH_SERVER;
  id:                           number = parseInt(sessionStorage.getItem('id'));
  profile:                      number = parseInt(sessionStorage.getItem('profile'));
  pets:                         any;
  petsResponse:                 any;
  timeNow:                      Date = new Date();
  dateNow:                      any = new Date();
  monthActual:                  Date = this.dateNow.getMonth() + 1;
  montMax:                      Date = new Date(this.dateNow.setMonth(this.monthActual) + 2);
  constructor(private authService: AuthService, private router: Router) {
    if (this.profile !== 1) {
      this.appointment = {
        id: this.id,
        name: '',
        typeAppointment: '',
        dateA: new Date(),
      };
    }
    this.appointmentInAttend = {
      attended: 'SI',
      result: '',
      medicated: '',
      descriptionM: '',
      treatment: '',
      descriptionTreatment: '',
      surgery: '',
      typeSurgery: '',
      descriptionS: '',
    };
  }

  public ngOnInit(): void {
    this.prevent();
  }

  prevent() {
    if (this.profile === 1) {
        this.onShowAppointmentsDoctor();
        return this.userA = true
    } else {
        this.onShowPets();
        this.onShowAppointments();
    }
  }

  onCreateAppointments(): void {
    // Crea una cita para los usuarios
    const appointment = this.appointment;
    console.log('appointment', appointment);
    for (const key in appointment) {
      if (appointment.hasOwnProperty(key)) {
        if (
          appointment[key] == '' ||
          appointment[key] == NaN ||
          appointment[key] == null
        ) {
          Swal.fire({
            icon: 'info',
            title: 'Tienes que llenar todos los campos',
            timer: 2000,
            showConfirmButton: false,
          });
          return;
        }
      }
    }

    const fecha:Date           = new Date(appointment.dateA)
    const fechaHora:number     = fecha.getHours();
    const fechaMinutos:number  = fecha.getMinutes();

    if (fechaHora < 8 || fechaHora > 18) {
      Swal.fire({
        icon: 'info',
        title: 'Información',
        text: 'La hora que elejiste no hay servicio de citas',
        showConfirmButton: true
      })
      return
    }
    if (!Number.isInteger(fechaMinutos / 15)) {
      Swal.fire({
        icon: 'info',
        title: 'Información',
        text: 'Tienes que escojer cada 15 minutos (0, 15, 30, 45)',
        showConfirmButton: true
      })
      return
    }
    this.authService.createAppointment(appointment)
      .subscribe(
        (res) => {
          Swal.fire({
            icon: 'success',
            title: 'Exito',
            text: 'Has creado una cita con exito',
            showConfirmButton: true,
          });
          this.appointment = {} as AppointmentI
          return this.onShowAppointments();
        },
        (error) => {
          if (error.error.status === 'info') {
            Swal.fire({
              icon: 'info',
              title: 'Recuerda que ...',
              text: error.error.message,
              showConfirmButton: true,
            });
            return;
          }
          Swal.fire({
            icon: 'error',
            title: 'Algo malo sucedio',
            text: error.error.message,
            showConfirmButton: true,
          });
          return;
        }
      );
  }

  closeDivs(divNumber: number): void {
    const divAppointmentDoctor  = document.getElementById('divAppointmentDoctor');
    const divSearchPet          = document.getElementById('divSearchPet');
    const divAttendPet          = document.getElementById('divAttendPet');
    const divAppointmentUser    = document.getElementById('divAppointmentUser');
    const divCreateAppointment  = document.getElementById('divCreateAppointment');
    const buttonShowForm        = document.getElementById('buttonShowForm');

    switch (divNumber) {
      case 1:
        buttonShowForm.hidden       = false;
        divCreateAppointment.hidden = true;
        divAppointmentUser.hidden   = false;
        break;
      case 2:
        buttonShowForm.hidden       = true;
        divCreateAppointment.hidden = false;
        divAppointmentUser.hidden   = true;
        break;
      case 3:
        divAppointmentDoctor.hidden = false;
        divSearchPet.hidden         = true;
        divAttendPet.hidden         = true;
        break;
      case 4:
        divAppointmentDoctor.hidden = true;
        divSearchPet.hidden         = false;
        divAttendPet.hidden         = true;
        this.onSearchPet('');
        break;
      case 5:
        divAppointmentDoctor.hidden = true;
        divSearchPet.hidden         = true;
        divAttendPet.hidden         = false;
        break;
      case 6:
        divAttendPet.hidden         = true;
        this.closeDivs(3);
        break;
      default:
        break;
    }
  }

  onShowPets() {
    // Consulta las mascotas para el usuario
    this.authService.showPet(this.id)
      .subscribe(
        (res) => {
          return this.pets = res.dataPet;
        },
        (error) => {
          if (error.error.status === 'error') {
            Swal.fire({
              icon: 'error',
              title: 'Error ...',
              text: 'Algo ha salido mal',
              showConfirmButton: true,
            });
            return;
          }
        }
      );
  }

  download(id: number, name: string): void{
    this.loading = true
    this.authService.downloadFile(id, name)
      .subscribe(
        (res) => {
          console.log('res', res)
          this.loading = false
          Swal.fire({
            icon: 'success',
            title: 'Exito',
            text: 'Descarga generada con exito',
            showConfirmButton: false,
            timer: 2000
          });
          return saveAs(res, `${name}`);
        },
        (error) => {
          this.loading = false
          Swal.fire({
            icon: 'info',
            title: 'Error',
            text: `${name} no tiene historial clínico`,
            showConfirmButton: false,
            timer: 2000
          });
        }
      )
  }
  onShowAppointments(): void {
    // Muestra las citas para el usuario
    this.authService.showAppointments(this.id)
      .subscribe(
        (res) => {
          if (res.dateAppointments) {
            console.log('res.dateAppointments', res.dateAppointments);
            return this.appointmentsUsers = res.dateAppointments;
          } else {
            Swal.fire({
              icon: 'info',
              titleText: res['message'],
            });
            return;
          }
        },
        (error) => {
          Swal.fire({
            icon: 'error',
            titleText: 'Error en el envio de datos al servidor',
          });
          return;
        }
      );
  }

  onShowAppointmentsDoctor(): void {
    this.loading = true;
    this.authService.showAppointmentsDoctor(this.id)
      .subscribe(
        (res) => {
          this.appointmentsDoctor = res.dateAppointments;
          if(!this.appointmentsDoctor){
            Swal.fire({
              icon: 'info',
              title: 'Información',
              text: 'No hay ninguna cita programada',
              showConfirmButton: true
            });;
            return false;
          }
          // Consulta las citas validando el id en el backend si tiene asociado el profile
          this.onShowPetsAppointmentsDoctor(res.dateAppointments);
          return this.loading = false;
        },
        (error) => {
          console.log('error', error);
        }
      );
  }

  onAssentAppointment(id: number, name: string, dateA: Date): void{
    const assentPet = { id, name, dateA };
    Swal.fire({
      title: '¿Estás seguro que el paciente no está?',
      text: '¡Una vez que lo hagas no hay vuelta atrás!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Estoy seguro'
    }).then((result) => {
      if (result.isConfirmed) {
        this.authService.assentAppointment(assentPet)
        .subscribe(
          (res) => {
            console.log('res', res);
            Swal.fire({
              icon: 'success',
              title: 'Lo hiciste bien',
              text: 'En otro momento sera :(',
              showConfirmButton: true
            });
            setTimeout(() => {
              this.onShowAppointmentsDoctor();
            }, 1000);
          },
          (error) => {
            console.log('error', error);
            Swal.fire({
              icon: 'error',
              title: 'Error',
              text: 'Error No se pudo cancelar',
              showConfirmButton: true
            });
          }
        )
      }
    })
  }

  onShowPetsAppointmentsDoctor(appointments: any): void {
    // Consulta los datos de la mascota con el id y el nombre de la mascota
    const datePetAppointment = appointments.map((item) => {
      return { id: item.id, name: item.name };
    });
    this.authService.showPetsAppointmentsDoctor(datePetAppointment)
    .subscribe(
      (res) => {
        console.log('res', res);
        return this.petsUserAppointments = res.dataPet;        
      },
      (error) => {
        console.log('errores', error);
      }
    );
  }
  onAttendPet(idP: number, nameP: string): boolean {
    this.petAttend = this.petsUserAppointments.find(({ id, name }) => {
      return id === idP && name === nameP;
    });
    this.appointmentAttend = this.appointmentsDoctor.find(({ id, name }) => {
      return id === idP && name === nameP;
    });
    this.closeDivs(5);
    return (this.activeAttend = true);
  }

  onAttendAppointment(): void {
    const veterinary: string = sessionStorage.getItem('name')
    this.petAttendDoctor = {
      ...this.petAttend,
      ...this.appointmentAttend,
      ...this.appointmentInAttend,
      veterinary
    };
    this.authService.updateAppointmentsDoctor(this.petAttendDoctor)
      .subscribe(
        (res) => {
          Swal.fire({
            icon: res['status'],
            title: 'Exito',
            text: res['message'],
            timer: 2000
          });
          setTimeout(() => {
            this.onShowAppointmentsDoctor();
            this.closeDivs(6);
          }, 1200);
        },
        (error) => {
          console.log('error', error);
        }
      );
  }

  watchPetAdmin(pet: Pet): void{
    Swal.fire({
      title: pet.name,
      imageUrl: `${this.url}/getImagePet/${pet.imageUrl}`,
      imageAlt: pet.name,
      imageWidth: 200,
      imageHeight: 180
    })
  }

  onSearchPet(namePet: any): void {
    this.loading = true;
    this.authService.searchPet(namePet)
      .subscribe(
        (res) => {
          this.loading        = false;
          this.foundPets      = true;
          this.petResultQuery = res.dataPet;
        },
        (error) => {
          this.loading    = false;
          this.foundPets  = false;  
        }
      );
  }
}
