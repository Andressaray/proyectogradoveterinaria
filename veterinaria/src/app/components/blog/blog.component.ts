import { Component, OnInit }  from '@angular/core';
import { Router }             from '@angular/router';
import Swal from 'sweetalert2';

import { AuthService }       from '../../services/service.service';
import { HeaderComponent }   from '../header/header.component';
import {Comment, upComment}                from '../../models/comment'

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {
  
  public comment:     Comment;
  public comments:    any;
  public updateCo:    upComment;
  public id:          number =  parseInt(sessionStorage.getItem('id')) ? parseInt(sessionStorage.getItem('id')): null;
  public name:        string = sessionStorage.getItem('name');
  public dates :      any;
  public loading:     Boolean = false;
  public isEdit:      Boolean = false;
  public profile:     number = sessionStorage.getItem('profile') ? parseInt(sessionStorage.getItem('profile')): 0;
  constructor(private authService: AuthService, private router: Router) { 
    this.comment = {
      id:           this.id,
      name:         this.name,
      calification: '',
      description:  '',
      profile:      this.profile
    }; 
    this.updateCo = {
      veterinary: this.name,
      messageResponse: ''
    }
  }

  ngOnInit(): void {
    this.onShowComments();
  }

  OnCreateComent(): void {
    this.comment.profile = this.profile;
    this.authService.comment(this.comment)
    .subscribe(
      (res) => {
        console.log('res', res);
        Swal.fire({
          icon: 'success',
          title: 'Exito',
          text: 'Tu comentario ha sido publicado',
          showConfirmButton: true
        });
        this.comment = {} as Comment
        this.onShowComments();
      }, 
      (error) => {
        console.log('error', error.error);
        Swal.fire({
          icon: 'info',
          text: error.error.message,
          showConfirmButton: true
        });
      });
  }
  onShowComments(): void {
    this.loading = true;
    this.authService.showComments()
    .subscribe(
      (res) => {
        this.loading = false;
        this.comments = res;
      },
      (err) => {
        Swal.fire({
          icon: 'info',
          text: 'No hay Comentarios',
          showConfirmButton: true
        });
        this.loading = false;
      }
    )
  }

  handleMultiply (value: number) {
    return '⭐'.repeat(value);
  }

  deleteComment(_id: Number, name: String){
    console.log('_id', _id);
    Swal.fire({
      title: '¿Estás seguro?',
      text: `Quieres eliminar el comentario de ${name}`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#28A745',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Estoy seguro',
    }).then((result) => {
      if (result.value) {
        this.authService.deleteComment(_id)
          .subscribe(
            (res) => {
              console.log('res', res);
              Swal.fire({
                icon: 'success',
                 title: 'Exito',
                 text: 'Eliminado con exito',
                 timer: 1200,
                 showConfirmButton: false
              })
              this.onShowComments()
            }, 
            (error) => {
              console.log('error', error);
          })
      }
    })
  }
  updateComment(_id){
    this.isEdit = false;
    this.updateCo.id_ = _id
    this.updateCo.timeResponse = Date.now()
    this.authService.updateOpinion(this.updateCo)
      .subscribe(
        (res) => {
          Swal.fire({
            icon: 'success',
             title: 'Exito',
             text: 'Has respondido al comentario',
             timer: 1200,
             showConfirmButton: false
          })
          this.onShowComments()
        },
        (error) => {

      })
  }
  preventEdit(){
    this.isEdit = true
  }
}
