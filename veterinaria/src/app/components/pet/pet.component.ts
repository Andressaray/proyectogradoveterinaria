import { Component, OnInit }  from '@angular/core';
import { Router }             from '@angular/router';
import { FormGroup, FormBuilder }          from '@angular/forms'
import Swal                   from 'sweetalert2';
import { ThemePalette }       from '@angular/material/core';

import { AuthService }        from '../../services/service.service';
import { listPets }           from '../../models/listPets'
import { Pet }                from 'src/app/models/pet';


@Component({
  selector: 'app-pets',
  templateUrl: './pet.component.html',
  styleUrls: ['./pet.component.css'],
})
export class PetComponent implements OnInit {
  public petU:          any;
  public pet:           Pet;
  public image:         File;
  public petExist:      Boolean = true;
  public petSelecteded: Pet;
  public color:         ThemePalette = 'primary';
  dateNow:              Date = new Date();
  minDate:              Date = new Date(`${(this.dateNow.getFullYear() - 20)}-${this.dateNow.getMonth()}-${this.dateNow.getDate()}`);
  user:                 boolean = false;
  raceReturn:           string [];
  pets:                 any;
  listRaces:            string [];
  listPetsJson:         listPets;
  url:                  string = this.authService.AUTH_SERVER;
  id:                   number = parseInt(sessionStorage.getItem('id'));
  nameP:                boolean;
  petUpdate:            FormGroup;
  petUpdateForm:        any;
  constructor(private authService: AuthService, private router: Router, private fb: FormBuilder) {
    this.pet = {
      id:         this.id,
      name:       '',
      race:       '',
      species:    '',
      gender:     '',
      date_born:  null,
      vaccinesO:  '',
      vaccines:   ''
    };
    this.petUpdateForm = {
      id:         this.id,
      name:       '',
      race:       '',
      species:    '',
      gender:     '',
      vaccinesO:  '',
      vaccines:   ''
    }
      
  }

  ngOnInit() {
    if (!sessionStorage.getItem('name')) {
      Swal.fire({
        icon: 'info',
        title: 'Tienes que iniciar sesion',
        showConfirmButton: false,
        timer: 2000,
      });
      this.router.navigateByUrl('/index');
    }
    this.prevent();
    this.getJson();
    this.onShowPets();
  }

  prevent() {
    const profile = sessionStorage.getItem('profile');
    if (profile == '1') {
      return (this.user = true);
    }
  }

  onChangeRace(race: string) {
    this.raceReturn = [...this.listPetsJson[race]];
  }

  petSelected(nameP: string): void{
    return this.petSelecteded = this.pets.find(({ name }) => name == nameP);
  }

  onHistorialPet(id: number, name: string, imageUrl:string): void {
    const petsHistorial = {
      id,
      name,
      imageUrl
    };
    this.authService.historialPet(petsHistorial)
      .subscribe(
        (res) => {
          console.log('res', res);
        },
        (error) => {
          console.log('error', error);
        }
      );
  }

  closeDivs(divNomber: number): void {
    const buttonShowCreatePet = document.getElementById('buttonShowCreatePet');
    const divShowPets         = document.getElementById('showPet');
    const divEditPet          = document.getElementById('divFormEditPet');
    const divFormCreatePets   = document.getElementById('divFormCreatePet');
    switch (divNomber) {
      case 1:
        {
          divFormCreatePets.hidden    = false;
          divShowPets.hidden          = true;
          buttonShowCreatePet.hidden  = true;
          divEditPet.hidden           = true;
        }
        break;
      case 2:
        {
          divShowPets.hidden          = false;
          buttonShowCreatePet.hidden  = false;
          divFormCreatePets.hidden    = true;
          divEditPet.hidden           = true;
        }
        break;
      case 3:
        {
          divEditPet.hidden         = false;
          divShowPets.hidden        = true;
          divFormCreatePets.hidden  = true;
          buttonShowCreatePet.hidden  = true;
        }
        break;
      case 4:
        {
          divEditPet.hidden           = true;
          divShowPets.hidden          = false;
          divFormCreatePets.hidden    = true;
          buttonShowCreatePet.hidden  = false;
        }
        break;
      case 5:
        {
          divEditPet.hidden           = true;
          divShowPets.hidden          = false;
          divFormCreatePets.hidden    = true;
          buttonShowCreatePet.hidden  = false;
        }
        break; 
      default:
        break;
    }
  }

  UpdatePet(namePet: string): void {
    this.closeDivs(3);
    this.petU = {
      ...this.pets.find(({ name }) => name === namePet)
    };
    this.petUpdateForm = {
      ...this.petU
    }
  }

  onShowPets() {
    this.authService.showPet(this.id)
      .subscribe(
        (res) => {
          if (Object.keys(res).length === 0) {
            return this.petExist = false;
          }
          console.log('res.dataPet', res.dataPet);
          return (this.pets = res.dataPet) && (this.petExist = true);
        },
        (error) => {
          console.log('error', error);
          Swal.fire({
            icon: 'error',
            title: 'No se pueden mirar tus mascotas',
            showConfirmButton: false,
            timer: 2000,
          });
        }
      );
  }

  onCreatePets(): void {
    const pet: Pet = {
      id:         this.id,
      name:       this.pet.name,
      race:       this.pet.race,
      species:    this.pet.species,
      gender:     this.pet.gender,
      date_born:  this.pet.date_born,
      vaccinesO:  this.pet.vaccinesO,
      vaccines:   this.pet.vaccines,
    };
    this.authService.createPet(pet)
        .subscribe(
          (res) => {
            if(this.image){
              this.authService.uploadImagePet(this.image, pet.id, pet.name)
                .subscribe(
                  (result) => {
                    Swal.fire({
                      icon: 'success',
                      title: 'Bien hecho',
                      text: 'Has creado a una nueva mascota',
                      showConfirmButton: true,
                    });
                    this.pet = {} as Pet
                    this.closeDivs(2)
                    return this.onShowPets();
                  }, 
                  (error) => {
                    Swal.fire({
                      icon: 'info',
                      title: 'Mmmmm ...',
                      text: 'La mascota se ha creado pero la imagen no se ha podido subir',
                      showConfirmButton: true,
                    });
                    this.pet = {} as Pet
                    this.closeDivs(2)
                    return this.onShowPets();
                })
            }else{
              Swal.fire({
                icon: 'success',
                title: 'Bien hecho',
                text: 'Has creado a una nueva mascota',
                showConfirmButton: true,
              });
              this.pet = {} as Pet
              this.closeDivs(2)
              return this.onShowPets();
            }
          },

          (error) => {
            console.log('error', error);
            let message =
              error.error.status === 'info'
                ? 'Recuerda que ...'
                : 'Algo ha ocurrido ...';
            Swal.fire({
              icon: error.error.status,
              title: message,
              text: error.error.message,
              showConfirmButton: true,
            });
          }
        );
  }

  getJson(): void {
    this.authService.getJson()
      .subscribe((res: listPets) => {
        this.listRaces = Object.keys(res);
        this.listPetsJson = res;
      });
  }
  onUploadImage(event): void {
    const file = event.target.files[0];
    const imageHtml = document.querySelector('#image')
    const image = URL.createObjectURL(file)
    imageHtml.setAttribute('src', image)
    const ext = file.name.split('.').pop();
    if (ext == 'png' || ext == 'jpg' || ext == 'jpeg' || ext == 'jfif') {
      let show = `<span>Imagen seleccionada: </span>${file.name}`;
      let output = document.getElementById('selector');
      output.innerHTML = show;
      output.classList.add('active');
      return this.image = event.target.files[0];
    }else{
      Swal.fire({
        icon: 'error',
        title: 'Error',
        text: 'El archivo seleccionado no es una imagen'
      });
    }
  }
  onUploadImageUpdate(event:  {target: HTMLInputElement}): File {
    const file: File = event.target.files[0];
    const ext: string  = file.name.split('.').pop();
    if (ext == 'png' || ext == 'jpg' || ext == 'jpeg' || ext == 'jfif') {
      let show = `<span>Imagen seleccionada: </span>${file.name}`;
      let output: HTMLElement = document.getElementById('imagePet');
      output.innerHTML = show;
      output.classList.add('active');
      return this.image = event.target.files[0];
    }else{
      Swal.fire({
        icon: 'error',
        title: 'Error',
        text: 'El archivo seleccionado no es una imagen'
      });
    }
  }

  onDeletePet(name: string, imageUrl: string = ''): void {
    const dataPet = {
      id:       this.id,
      name:     name,
      imageUrl: imageUrl
    };
    Swal.fire({
      title: '¿Estás seguro?',
      text: `Si borras a tu mascota ${dataPet.name}, se eliminaran todas las citas e historial clinico`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#28A745',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Estoy seguro',
    }).then((result) => {
      if (result.value) {
        return this.authService.deletePet(dataPet)
          .subscribe(
            (res) => {
              Swal.fire({
                icon: 'success',
                title: 'Mascota borrada con exito',
                showConfirmButton: false,
                timer: 1500,
              });
              setTimeout(() => {
                this.onShowPets();
              }, 1000);
              return;
            },
            (error) => {
              console.log('error', error);
              Swal.fire({
                icon: 'error',
                title: 'No se pudo eliminar la mascota',
                showConfirmButton: false,
                timer: 1500,
              });
              return;
            }
          );
      }
    });
  }

  onUpdatePet(): void {      
    const petEdit = {
      petBefore: {...this.petU},
      petNow: {...this.petUpdateForm}
    }
    Swal.fire({
      title: '¿Estás seguro?',
      text: 'Si cambias los datos de tu mascota, no habra vuelta atrás',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#28A745',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Estoy seguro',
    })
      .then((result) => {
        if (result.value) {
          this.authService.updatePet(petEdit)
          .subscribe(
            (res) => {
                if(this.image){
                  console.log('this.image', this.image);
                  this.authService.uploadImagePet(this.image, this.id, this.petUpdateForm.name)
                  .subscribe(
                    (result) => {
                      Swal.fire({
                        icon: 'success',
                        title: 'Exito',
                        text: 'Mascota actualizada con exito',
                        timer: 1200
                      });
                      this.closeDivs(5);
                      this.onShowPets();
                      return;
                    }, 
                    (error)  => {
                      Swal.fire({
                        icon: 'error',
                        title: 'Error',
                        text: 'No se ha podido actualizar tu mascota',
                        timer: 1200
                      });
                  })
                }
                Swal.fire({
                  icon: 'success',
                  title: 'Exito',
                  text: 'Mascota actualizada con exito',
                  timer: 1200
                });
                this.closeDivs(5);
                this.onShowPets();
                return;
              }, 
              (error) => {
                Swal.fire({
                  icon: 'error',
                  title: 'Error',
                  text: 'No se ha podido actualizar tu mascota',
                  timer: 1200
                });
            });
        }
      });
  }


  uploadImageResponse(data): void {
    Swal.fire({
      icon: 'success',
      text: 'Imagen actualizada con exito',
      showConfirmButton: false,
      timer: 3000,
    });
    setTimeout(() => {
      this.onShowPets();
    }, 3000);
  }

  getImage(nameFile: string): void {
    console.log('nameFile', nameFile);
    this.authService.getImagePet(nameFile)
      .subscribe(
        (response) => {
          console.log('exito');
        },
        (error) => {
          console.log('error');
        }
      );
  }
}
