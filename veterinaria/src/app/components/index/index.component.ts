import { Component, OnInit } from '@angular/core';
import { Router }            from '@angular/router';

import { HeaderComponent }   from '../header/header.component';                                                  

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {

  }

}
