import { Component } from '@angular/core';
import { Location }  from '@angular/common';
import { Router }    from '@angular/router';

@Component({
  selector: 'app-header',
  template: `
    <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-primary mb-5">
      <a class="navbar-brand" [routerLink]="['../index']">Inicio</a>
      <button
        class="navbar-toggler bg-secondary"
        type="button"
        data-toggle="collapse"
        data-target="#navbarTogglerDemo02"
        aria-controls="navbarTogglerDemo02"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
          <li class="nav-item active">
              <a class="nav-link" [routerLink]="['../blog']">Blog</a>
          </li>
          <li class="nav-item active">
              <a class="nav-link" [routerLink]="['../about']">Nosotros</a>
          </li>
          <ng-container *ngIf="this.user; else noLogin">
            <ng-container *ngIf="this.profile !== 1">
              <li class="nav-item active">
                <a class="nav-link" [routerLink]="['../pet']">Mis Mascotas</a>
              </li>
            </ng-container>
            
            <li class="nav-item active">
              <a class="nav-link" [routerLink]="['../appointment']">Citas</a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" [routerLink]="['../logout']">Cerrar Sesion</a>
            </li>
          </ng-container>
          <ng-template #noLogin>
            <li class="nav-item active">
              <a class="nav-link" [routerLink]="['../login']">Iniciar Sesion</a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" [routerLink]="['../register']">Registro</a>
            </li>
          </ng-template>
        </ul>
      </div>
    </nav>
  `,
  styles: ['ul{list-style: none;}'],
})
export class HeaderComponent {
  user: boolean = false;
  urlActive: String;
  public profile: number = sessionStorage.getItem('profile') ? parseInt(sessionStorage.getItem('profile')): 0;
  constructor(private location: Location, private router: Router) {
    this.componentActive();
    this.preventNav();
  }
  preventNav(){
    const id = sessionStorage.getItem('id');
    return id ? (this.user = true) : (this.user = false);
  }
  componentActive():void{
    this.urlActive = this.router.url;
  }
}
