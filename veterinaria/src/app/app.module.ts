import { BrowserModule }                    from '@angular/platform-browser';
import { LOCALE_ID, NgModule }              from '@angular/core';
import { BrowserAnimationsModule }          from '@angular/platform-browser/animations';
import { CUSTOM_ELEMENTS_SCHEMA }           from '@angular/core';
import localeEsCo                           from '@angular/common/locales/es-CO';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes }             from '@angular/router';
import { registerLocaleData }               from '@angular/common';
import { HttpClientModule }                 from '@angular/common/http'; 

import { OrderModule }                      from 'ngx-order-pipe';
import { MatDatepickerModule }              from '@angular/material/datepicker';
import { MatNativeDateModule }              from '@angular/material/core';
import { MatInputModule }                   from '@angular/material/input';
import { MatButtonModule }                  from '@angular/material/button';
import { MatIconModule }                    from '@angular/material/icon';
import { MatFormFieldModule }               from '@angular/material/form-field';
import { MatSelectModule }                  from '@angular/material/select';
import { MatRadioModule }                   from '@angular/material/radio';
import { NgxMatDatetimePickerModule, 
  NgxMatNativeDateModule, 
  NgxMatTimepickerModule  
       }                                    from '@angular-material-components/datetime-picker';


import { AppComponent }         from './app.component';
import { LoginComponent }       from './components/login/login.component';
import { RegisterComponent }    from './components/register/register.component';
import { ErrorComponent }       from './components/error/error.component';
import { IndexComponent }       from './components/index/index.component';
import { AppointmentComponent } from './components/appointment/appointment.component';
import { BlogComponent }        from './components/blog/blog.component';
import { LogoutComponent }      from './components/logout/logout.component';
import { PetComponent }         from './components/pet/pet.component';
import { AboutComponent }       from './components/about/about.component';
import { HeaderComponent }      from './components/header/header.component';
import { ChatComponent }        from './components/chat/chat.component';
import { AuthService }          from './services/service.service';
import { RecoverComponent }     from './components/recover/recover.component';

registerLocaleData(localeEsCo, 'es-CO');

const routes: Routes = [
  { path:  '',            component: IndexComponent },
  { path:  'index',       component: IndexComponent }, 
  { path:  'pet',         component: PetComponent },
  { path:  'blog',        component: BlogComponent },
  { path:  'about',       component: AboutComponent },
  { path:  'login',       component: LoginComponent },
  { path:  'register',    component: RegisterComponent },
  { path:  'appointment', component: AppointmentComponent },
  { path:  'logout',      component: LogoutComponent },
  { path:  'chat',        component: ChatComponent  },
  { path:  'recover',     component: RecoverComponent },
  { path:  '**',          component: ErrorComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    IndexComponent,
    AppointmentComponent,
    BlogComponent,
    LogoutComponent,
    PetComponent,
    HeaderComponent,
    AboutComponent,
    ChatComponent,
    ErrorComponent,
    RecoverComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatRadioModule,
    MatButtonModule,
    OrderModule,
    MatIconModule,
    MatDatepickerModule,
    NgxMatDatetimePickerModule,
    NgxMatTimepickerModule,
    NgxMatNativeDateModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes)
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [AuthService, { provide: LOCALE_ID, useValue: 'es-CO' }],
  bootstrap: [AppComponent]
})
export class AppModule { }
