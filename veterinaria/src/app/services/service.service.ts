import { Injectable }                  from '@angular/core';
import { HttpClient }                  from '@angular/common/http';
import { HttpHeaders }                 from '@angular/common/http';

import { tap }                         from 'rxjs/operators';
import { Observable, BehaviorSubject } from 'rxjs';
import Swal                            from 'sweetalert2';

import { User, UserI, UserN }                 from '../models/user';
import { UserD }                       from '../models/userD';
import { JwtResponseI }                from '../models/jwt-response';
import { JwtResponsePets }             from '../models/jwt-response-pets';
import { JwtResponseAppointments }     from '../models/jwt-response-Appointments';
import { AppointmentI }                from '../models/appointment';
import { AppointmentsA }               from '../models/appointmentA';
import { Pet, PetI }                   from '../models/pet';
import { PetD }                        from '../models/petD';
import { PetU }                        from '../models/petU';
import { Comment }                     from '../models/comment';

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  readonly AUTH_SERVER: string   = 'http://localhost:3000';
  url         : string  = '../../assets/listPets.json';
  authSubject           = new BehaviorSubject(false);
  private token: string;
  constructor(private httpClient: HttpClient) { }

  //Users
  register(user: UserN): Observable <JwtResponseI> {
    return this.httpClient.post<JwtResponseI>(`${this.AUTH_SERVER}/register`,user)
      .pipe(tap(
        (res: JwtResponseI) => {
          if (res) {
            // guardar token
            this.saveToken(res.dataUser.accessToken, res.dataUser.expiresIn, res.dataUser.name, res.dataUser.id, null);
          }
        })
      );
  }

  login(user: User): Observable <JwtResponseI> {
    try {
      return this.httpClient.post<JwtResponseI>(`${this.AUTH_SERVER}/login`,
        user).pipe(tap(
          (res: JwtResponseI) => {
            if (res) {
              // guardar token
              if (res.dataUser.profile) {
                this.saveToken(res.dataUser.accessToken, res.dataUser.expiresIn, res.dataUser.name, res.dataUser.id, res.dataUser.profile);
              }
              else {
                this.saveToken(res.dataUser.accessToken, res.dataUser.expiresIn, res.dataUser.name, res.dataUser.id, null);
              }
            }
          })
        );
    } catch (error) {
      Swal.fire({
        icon: 'error',
        title: error.message,
        showConfirmButton: false,
        timer: 1500
      });
    }
  }

  validateEmail(email: string) {
    return this.httpClient.get(`${this.AUTH_SERVER}/validate`);
  }


//Pets
  createPet(pet: Pet): Observable <JwtResponsePets> {
    return this.httpClient.post<JwtResponsePets>(`${this.AUTH_SERVER}/pets`,
      pet).pipe(tap());
  }

  uploadImagePet(imageUrl: File, id: number, name: string): Observable <Object> {
    const form = new FormData();
    form.append('imageUrl', imageUrl);
    form.append('id', id.toString());
    form.append('name', name);
    return this.httpClient.post(`${this.AUTH_SERVER}/uploadImagePet`, form);
  }
  
  getImagePet(nameFile: string){
    return this.httpClient.get(`${this.AUTH_SERVER}/getImagePet/${nameFile}`);
  }
  
  showPet(id: number){
    return this.httpClient.get<JwtResponsePets>(`${this.AUTH_SERVER}/showPets${id}`);
  }

  deletePet(pet: PetD){
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: {
        'id':       pet.id,
        'name':     pet.name,
        'imageUrl': pet.imageUrl
      },
    };
    return this.httpClient.delete(`${this.AUTH_SERVER}/deletePets`, options)
  }

  updatePet(pet: any): Observable <JwtResponsePets> {
    return this.httpClient.put<JwtResponsePets>(`${this.AUTH_SERVER}/updatePets`, pet);
  }

  getJson() {
    return this.httpClient.get(this.url);
  }

  historialPet(pet: PetD):Observable <JwtResponsePets>{
    return this.httpClient.post<JwtResponsePets>(`${this.AUTH_SERVER}/historialPets`, pet);
  }
//Appointments

  createAppointment(appointment: AppointmentI): Observable <JwtResponseAppointments> {
    return this.httpClient.post<JwtResponseAppointments>(`${this.AUTH_SERVER}/appointments`, appointment);
  }

  showAppointments(id: number){
    return this.httpClient.get<AppointmentsA>(`${this.AUTH_SERVER}/showAppointments${id}`);
  }

  // Comment

  comment(comment: Comment) {
    return this.httpClient.post(`${this.AUTH_SERVER}/createComment`, comment);
  } 

  showComments() {
    return this.httpClient.get(`${this.AUTH_SERVER}/showComments`);
  }

  deleteComment(id_: Number){
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: {
        'id_':       id_,
      },
    };
    return this.httpClient.delete(`${this.AUTH_SERVER}/deleteOpinion`, options)
  }

  updateOpinion(comment: any){
    return this.httpClient.put(`${this.AUTH_SERVER}/updateOpinion`, comment)
  }

  //ProfileDoctor

  searchPet(pet: string): Observable<JwtResponsePets> {
    return this.httpClient.post<JwtResponsePets>(`${this.AUTH_SERVER}/showPetsDoctor`, pet);
  }

  showPetsAppointmentsDoctor(pet: any){
    return this.httpClient.post<JwtResponsePets>(`${this.AUTH_SERVER}/showPetsAppointmentsDoctor`, pet);
  }

  showAppointmentsDoctor(id: number){
    return this.httpClient.post<JwtResponseAppointments>(`${this.AUTH_SERVER}/showAppointmentsDoctor`, {id});
  }

  updateAppointmentsDoctor(appointment: AppointmentsA): Observable<JwtResponseAppointments>{
    return this.httpClient.put<JwtResponseAppointments>(`${this.AUTH_SERVER}/updateAppointmentsDoctor`, appointment)
  }

  assentAppointment(pet: any): Observable<PetD> {
    return this.httpClient.put<PetD>(`${this.AUTH_SERVER}/assentAppointmentDoctor`, pet);
  }

  downloadFile(id: number, name: string){
    const header = { 
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        responseType: "blob"
      })
    }
    return this.httpClient
      .get(`${this.AUTH_SERVER}/download${id}/${name}`, {responseType: "blob" })
  }

//sessionStorage

  logout(id: UserD): Observable <JwtResponseI> {
    this.token = '';
    sessionStorage.removeItem('ACCESS_TOKEN');
    sessionStorage.removeItem('EXPIRES_IN');
    sessionStorage.removeItem('name');
    sessionStorage.removeItem('id');
    sessionStorage.removeItem('profile');
    return this.httpClient.post<JwtResponseI>(`${this.AUTH_SERVER}/logout`, id);
  }

  private saveToken(token: string, expiresIn: string, name: string, id: number, profile: string): void {
    // sessionStorage.setItem('ACCESS_TOKEN', token);
    sessionStorage.setItem('EXPIRES_IN', expiresIn);
    sessionStorage.setItem('name', name);
    sessionStorage.setItem('id', String(id));
    if(profile){
      sessionStorage.setItem('profile', profile);
    }
    this.token = token;
  }

  private getToken(): string {
    if (!this.token) {
      this.token = sessionStorage.getItem('ACCESS_TOKEN');
    }
    return this.token;
  }

}