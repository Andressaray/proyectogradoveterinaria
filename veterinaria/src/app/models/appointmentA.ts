export interface AppointmentsA {
    dateAppointments: {
        id:                     number,
        name:                   string,
        date:                   Date,
        typeAppointment:        string,
        attended:               string,
        result:                 string,
        medicated:              string,
        descriptionM:           string,
        treatment:              string,
        descriptionTreatment:   string,
        surgery:                string,
        typeSurgery:            string
    }
}