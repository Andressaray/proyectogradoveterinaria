export interface Comment {
    id:             number,
    name:           string,
    calification:   string,
    description:    string,
    profile:        number
}

export interface upComment {
    id_?:               number,
    veterinary:         string,
    messageResponse:    string,
    timeResponse?:      number

}