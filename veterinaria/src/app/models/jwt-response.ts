export interface JwtResponseI {
  dataUser: {
    id:           number,
    name:         string,
    email:        string,
    profile:      string,
    accessToken:  string,
    expiresIn:    string
  }
}
