export interface PetU {
    id:         number,
    name:       string,
    nameU:      string,
    race:       string,
    species:    string,
    gender:     string,
    date_born:  Date,
    vaccinesO:  string,
    vaccines:   string
}