export interface PetI {
    id:         number
}

export interface Pet {
    id:         number,
    name:       string,
    race:       string,
    species:    string,
    gender:     string,
    date_born:  Date,
    vaccinesO:  string,
    vaccines:   string,
    imageUrl?:  string
}
