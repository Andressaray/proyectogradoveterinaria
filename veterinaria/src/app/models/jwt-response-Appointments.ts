export interface JwtResponseAppointments {
    dateAppointments: [
        {
            id:                 number,
            name:               string,
            date:               Date,
            typeAppointment:    string
        }
    ]
}