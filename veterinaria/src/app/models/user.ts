export interface UserI {
  id:       number,
  name:     string,
  email:    string,
  profile:  string,
  password: string
}

export interface User {
  email:    string|number,
  password: string
}

export interface UserN {
  id:       number,
  name:     string,
  lastname: string,
  phone:    number,
  email:    string,
  password: string 
}