export interface AppointmentI{
    id:                     number,
    name:                   string,
    dateA:                  Date,
    typeAppointment:        string,
}