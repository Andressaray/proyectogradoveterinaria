const Users         = require('./auth.controller');
const Pets          = require('./pet.controller');
const Opinions      = require('./opinion.controller');
const Appointments  = require('./appointmen.controller');
const storage       = require('../config/multer');
const multer        = require('multer');
const path          = require('path');
const uploader      = multer({
  storage,
  fileFilter: (req, file, cb) => {
    let ext = path.extname(file.originalname)
    if(ext === '.png' || ext === '.jpg' || ext === '.jpeg' || ext === '.jfif') {
      cb(null, `${file.originalname}`);
    }
    return cb(null, false, new Error('No es una extension de imagen '));
  }
}).single('imageUrl');

module.exports  = (router) => {
  router.post('/register', Users.createUser);
  router.post('/login', Users.loginUser);
  router.post('/logout', Users.signout);
  
  router.post('/pets', Pets.createPets);
  router.get('/showPets:id', Pets.showPets);
  router.delete('/deletePets', Pets.deletePets);
  router.put('/updatePets', Pets.updatePets);
  router.post('/uploadImagePet', uploader, Pets.uploadImage);
  router.get('/getImagePet/:nameFile', Pets.getImagePet);

  router.post('/appointments', Appointments.createAppointments);
  router.get('/showAppointments:id', Appointments.showAppointments);
  router.get('/download:id/:name', Appointments.download);
  
  router.post('/createComment', Opinions.opinionCreateUsers);
  router.get('/showComments', Opinions.showOpinion);

  // Administrador
  router.post('/showPetsDoctor', Pets.showPetsDoctor);
  router.post('/showPetsAppointmentsDoctor', Appointments.showPetsAppointmentsDoctor);
  router.post('/showAppointmentsDoctor', Appointments.showAppointmentsDoctor);
  router.put('/assentAppointmentDoctor', Appointments.assentAppointmentDoctor);
  router.put('/updateAppointmentsDoctor', Appointments.updateAppointmentsDoctor);
  router.delete('/deleteOpinion', Opinions.deleteOpinion);
  router.put('/updateOpinion', Opinions.updateOpinion);
}