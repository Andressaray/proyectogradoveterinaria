
const Pet           = require('./auth.dao.pets');
const Appointment   = require('./auth.dao.appointments');
const User          = require('./auth.controller');
const path          = require('path');
const fs            = require('fs');
const folderImages  = './public/images/';
const _             = require('lodash');
const cloudinary    = require('cloudinary').v2;

cloudinary.config({
  cloud_name: 'villavicencio',
  api_key: '686436375531498',
  api_secret: 'zxlnPDVWbzRxen_r-YpwDz09T2g'
})

const cloudImage = async (nameFile) => {
  console.log('nameFile', nameFile);
  const message = await cloudinary.uploader.upload(nameFile)
  console.log('message', message);
}

exports.uploadImage = async (req, res) => {
    try {
      const file              = req;
      const { id, name }      = req.body;
      const { originalname }  = file.file;
      await cloudImage(file.file.path)
      if (file && id && name) {
        await Pet.findOne({ id, name }, async (err, petBefore) => {
          if (err || !petBefore) {
            res.status(404).send({
              status:   'info',
              message:  'La mascota no existe',
            });
            res.end();
            return;
          } else {
            const fileNameBefore = petBefore.imageUrl;
            if (originalname != fileNameBefore) {
              const contentFileNameBefore = folderImages + fileNameBefore;
              fs.unlink(contentFileNameBefore, (exist) => {
                // console.log('exist', exist);
              });
            }
          }
        });
        await Pet.findOneAndUpdate(
          { id, name},
          {
            $set: {
              imageUrl: originalname,
            },
          },
          async (erro, imageUrl) => {
            if (erro || !imageUrl) {
              res.status(404).send({
                status:   'info',
                message:  'La mascota no existe',
              });
              res.end();
              return;
            } else {
              res.status(200).send({
                status:   'success',
                message:  'Imagen guardada con exito',
              });
              res.end();
              return;
            }
          }
        );
      } else {
        res.status(404).send({
          status:   'info',
          message:  'No hay imagen añadida',
        });
        return;
      }
    } catch (error) {
      res.status(404).send({
        status:   'info',
        message: 'No hay imagen añadida',
      });
      return;
    }
  };
  
  exports.getImagePet = async (req, res) => {
    const nameFile = req.params.nameFile;
    if (nameFile) {
      let pathFile = folderImages + nameFile;
      fs.exists(pathFile, (exists) => {
        if (exists) {
          res.sendFile(path.resolve(pathFile));
          return;
        } else {
          res.status(404).send({
            status:   'error',
            message:  'La imagen no existe',
          });
          return;
        }
      });
    }
  };
  
  exports.createPets = async (req, res) => {
    try {
      const newPet = {
        id:         parseInt(req.body.id),
        name:       req.body.name,
        race:       req.body.race,
        species:    req.body.species,
        gender:     req.body.gender,
        date_born:  req.body.date_born,
        vaccinesO:  req.body.vaccinesO,
        vaccines:   req.body.vaccines,
      };
      const userExist = await User.preventUserInvalid(newPet.id);
      if (userExist) {
        const petsUser = await Pet.countDocuments({ id: newPet.id });
        if (petsUser === 5) {
          res.status(409).send({
            status: 'info',
            message: 'El limite de mascotas es 5',
          });
          return;
        }
        const loopDatesPet = await loopDataPet(newPet);
        if (loopDatesPet) {
          res.status(409).send({
            status: 'error',
            message: loopDatesPet,
          });
          return;
        }
        const petInvalid = await preventPetInvalid(newPet.id, newPet.name);
        console.log('petInvalid', petInvalid);
        if (petInvalid) {
          res.status(403).send({
            status:   'info',
            message:  'Esta mascota ya existe',
          });
          return;
        }
        senDatesPets();
        async function senDatesPets () {
          Pet.createPet(newPet, async (err, pet) => {
          if (err && err.code == 11000) {
            res.status(409).send({
              status:   'info',
              message:  'Esta mascota ya existe',
            });
            return;
          }
          if (err) {
            res.status(500).send({
              status:   'error',
              message:  'Error en el envio de datos al servidor',
            });
            return;
          }
          const dataPet = {
            id:         pet.id,
            name:       pet.name,
            race:       pet.race,
            species:    pet.species,
            gender:     pet.gender,
            date_born:  pet.date_born,
            vaccinesO:  pet.vaccinesO,
            vaccines:   pet.vaccines,
          };
          await res.send({ dataPet });
          return;
        });
      }
      } else {
        res.status(409).send({
          status:   'error',
          message:  'Este usuario no existe',
        });
      }
    } catch (error) {
      res.status(500).send({
        status:   'error',
        message:  'Error el servidor no pudo procesar los datos',
      });
      return;
    }
  };
  
  exports.showPets = async (req, res) => {
    if (!req) {
      res.status(500).send({
        status:   'error',
        message:  'Error en el envio de datos al servidor',
      });
      return;
    }
    const petList = await Pet.find({ id: req.params.id });
    let dataPet = [];
    _.forEach(petList, (value, index) => {
      dataPet[index] = {
        id:         value.id,
        name:       value.name,
        race:       value.race,
        species:    value.species,
        gender:     value.gender,
        date_born:  value.date_born,
        vaccinesO:  value.vaccinesO,
        vaccines:   value.vaccines,
        imageUrl:   value.imageUrl,
      };
    });
    if (Object.keys(dataPet).length === 0) return res.send(dataPet);
    await res.send({ dataPet });
    return;
  };
  
  exports.deletePets = async (req, res) => {
    try {
      const petDelete = {
        id:       parseInt(req.body.id),
        name:     req.body.name,
        imageUrl: req.body.imageUrl
      };
      const deletePet = await  handleDeletePet(petDelete.id, petDelete.name)
      petDelete.imageUrl !== '' && handleDeletePhoto(petDelete.imageUrl)
      if(deletePet){
        res.status(200).send({
          status: 'success',
          message: 'Mascota eliminada con exito'
        })
        return;
      }else{
          res.status(404).send({
            status: 'error',
            message: 'No se pudo eliminar la mascota'
          })
          return;
      } 
    } catch (error) {
      res.status(500).send({
        status: 'error',
        message: 'No se pudo eliminar la mascota'
      })
      return;
    }
  };

exports.showPetsDoctor = async (req, res) => {
    if (!req) return res.status(500).send('Server error');
    const { namePet } = req.body;
    let namePets    = ''
    namePet ? namePets = namePet.toUpperCase() : namePets = ''
    const dataPet     = await Pet.find({ name: { $regex: `^${namePets}` } });
    if (_.isEmpty(dataPet)) {
      res.status(404).send({
        status: 'error',
        message: 'No existe ninguna mascota con ese nombre',
      });
      return;
    }
    await res.send({ dataPet });
    return;
  };
  
  
  exports.updatePets = (req, res) => {
    const { petBefore, petNow } = req.body;
    if(!_.isEqual(petBefore, petNow)){
      Pet.findOneAndUpdate(
      { id: petBefore.id, name: petBefore.name },
        {
        $set: {
            name:         petNow.name,
            race:         petNow.race,
            species:      petNow.species,
            gender:       petNow.gender,
            date_born:    petNow.date_born,
            vaccinesO:    petNow.vaccinesO,
            vaccines:     petNow.vaccines
          },
      },
      async (err, petU) => {
        if (err) {
          res.status(500).send({
            status:   'error',
            message:  'Erro en el envio de datos al servidor',
          });
          return;
        }
        if (!petU) {
          res.status(409).send({
            status:   'error',
            message:  'La mascota no existe',
          });
          return;
        } else {
          res.send({
            status:   'success',
            message:  'Mascota actualizada con exito',
          });
        }
      }
    );
    }
  };

const preventPetInvalid = async (id, name) => {
    const pet = await Pet.findOne({ id, name }, (err, pet) => {
        if (err && err.code == 11000) {
        return 'error';
        }
        if (pet) {
        return 'Ya existe';
        } else {
        return pet;
        }
    });
    return pet;
};

const handleCreateAppointment = async (newAppointment) => {
    let appoinmentUser = {}
    Appointment.createAppintment(
      newAppointment,
      async (err, appoinments) => {
        if (err) {
          return (error = 'Error en el envio de datos al servidor')
        } else {
          appoinmentUser = {
            id:               appoinments.id,
            name:             appoinments.name,
            date:             appoinments.dateA,
            typeAppointment:  appoinments.typeAppointment
          };
        }
      });
      return appoinmentUser;
  }

const handleDeletePhoto = (photo) => {
    fs.unlink(folderImages+photo, (exist) => {
      if(exist){
        return 'La imagen no fue encontrada'
      }
    })
  }
  
  const handleDeletePet = async (id, name) => {
    try {
      const petPromise =  await
      Pet.findOneAndDelete(
        { $and: [{ id }, { name }] }) 

      const appointmentAttend = await
      Appointment.findOneAndDelete(
        {
          $and: [{ id }, { name }, { attended: 'NO' }]
        }
      )
      return petPromise;
    } catch (error) {
        throw error = 'Error, no se encontro la mascota'
    }
}

const loopDataPet = async (newPet) => {
  let error = ''; 
  _.forEach(newPet, (value, index) => {
    if (_.isUndefined(value) || _.isEmpty(value)) {
      if (!newPet['vaccinesO'] == 'No') {
        return (error = `Falta llenar el campo ${index}`);
      }
    }
  });
  return error;
}