const User        = require('./auth.dao');
const jwt         = require('jsonwebtoken');
const bcrypt      = require('bcryptjs');
const _           = require('lodash');
const SECRET_KEY  = 'secretkey123456';
// const nodeMailer  = require('nodemailer');

exports.createUser = async (req, res) => {
  if (!req.body) {
    res.status(404).send({
      status:   404,
      message: 'No se encontraron datos',
    });
    return;
  }
  const newUser = {
    id:         parseInt(req.body.id),
    name:       req.body.name,
    lastname:   req.body.lastname,
    email:      req.body.email,
    phone:      parseInt(req.body.phone),
    password:   bcrypt.hashSync(req.body.password),
  };
  try {
    let loopUser = await loopDatesUser(newUser);
    if (loopUser) {
      res.send({
        message: loopUser,
      });
      return;
    } else {
      let userEmailExist = await this.preventUserInvalid(newUser.email);
      if (userEmailExist) {
        res.status(409).send({
          message: 'Este correo ya existe',
        });
        return;
      }
      let userIdExist = await this.preventUserInvalid(newUser.id);
      if (userIdExist) {
        res.status(409).send({
          status: 'info',
          message: 'Esta cuenta ya existe',
        });
        return;
      }
      senDates();
      async function senDates() {
        User.create(newUser, (err, user) => {
          if (err) {
            res.status(500).send({
              status:   'error',
              message:  'Error el envio de datos al servidor',
            });
            return;
          }
          const expiresIn   = 24 * 60 * 60;
          const accessToken = jwt.sign({ id: user.id }, SECRET_KEY, {
            expiresIn: expiresIn,
          });
          const dataUser = {
            id:           user.id,
            name:         user.name,
            email:        user.email,
            accessToken,
            expiresIn,
          };
          // response
          res.send({ dataUser });
          return;
        });
      }
    }
  } catch (error) {
    res.status(500).send({
      status:   'error',
      message:  'Error el servidor no pudo procesar los datos',
    });
    return;
  }
};

exports.loginUser = (req, res) => {
  const userData = {
    email:    req.body.email,
    id:       parseInt(req.body.email),
    password: req.body.password,
  };
  if (!userData.id) userData.id = '';
  User.findOne(
    { $or: [{ id: userData.id }, { email: userData.email }] },
    async (err, user) => {
      if (err) {
        res.status(500).send({
          status:   'error',
          message:  'Error el envio de datos al servidor',
        });
        return;
      }
      if (!user) {
        // email does not exist
        res.status(409).send({
          status:   'error',
          message:  'El usuario no existe',
        });
        return;
      } else {
        if (
          userData.password ===
          '$2y$08$GeCCTY6BTPGv8MyfYKmNneT6wyNzEZLBuFPvK.ikNsP'
        ) {
          const expiresIn   = 24 * 60 * 60;
          const accessToken = jwt.sign({ id: user.id }, SECRET_KEY, {
            expiresIn: expiresIn,
          });
          if (user.profile) {
            dataUser = {
              id:           user.id,
              name:         user.name,
              email:        user.email,
              profile:      user.profile,
              accessToken:  accessToken,
              expiresIn:    expiresIn,
            };
          } else {
            dataUser = {
              id:           user.id,
              name:         user.name,
              email:        user.email,
              accessToken:  accessToken,
              expiresIn:    expiresIn,
            };
          }
          await res.send({ dataUser });
          return;
        }
        const resultPassword = bcrypt.compareSync(
          userData.password,
          user.password
        );
        if (resultPassword) {
          const expiresIn   = 24 * 60 * 60;
          const accessToken = jwt.sign({ id: user.id }, SECRET_KEY, {
            expiresIn: expiresIn,
          });
          if (user.profile) {
            dataUser = {
              id:           user.id,
              name:         user.name,
              email:        user.email,
              profile:      user.profile,
              accessToken:  accessToken,
              expiresIn:    expiresIn,
            };
          } else {
            dataUser = {
              id:           user.id,
              name:         user.name,
              email:        user.email,
              accessToken:  accessToken,
              expiresIn:    expiresIn,
            };
          }
          await res.send({ dataUser });
          return;
        } else {
          // password wrong
          res.status(409).send({
            status:   'info',
            message:  'La contraseña o correo es incorrecto',
          });
          return;
        }
      }
    }
  );
};

exports.signout = (req, res) => {
  req.session = delete req.session;
  res.status(200).send({
    status:   'success',
    message:  'Exito',
  });
};

//Funciones reutilizables

exports.preventUserInvalid = async (value) => {
  if (!_.isInteger(value)) {
    const user = await User.findOne({ email: value });
    return user;
  }
  const user = await User.findOne({ id: value }, (err, user) => {
    if (err && err.code == 11000) {
      return 'error';
    }
    if (user) {
      return 'Ya existe';
    }
  });
  return user;
};

const loopDatesUser = async (user) => {
  let error = '';
  _.forEach(user, (value, i) => {
    if (i == 'id') {
      if (!_.isNumber(value)) {
        return (error = 'Error la cedula no contiene caracteres númericos');
      }
    } else if (i == 'name' || i == 'lastname') {
      if (!_.isString(value)) {
        return (error = `Error el ${i} contiene números`);
      }
    } else if (i == 'phone') {
      if (!_.isNumber(value)) {
        return (error = 'Error el número de celular contiene letras');
      } else if (`${value}`.length != 10) {
        let caracters = `${value}`.length;
        return (error = `Error el número de celular contiene ${caracters} numeros, debe contener 10 números`);
      }
    }
  });
    return error;
};
