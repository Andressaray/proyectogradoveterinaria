const Appointment       = require('./auth.dao.appointments');
const Pet               = require('./auth.dao.pets');
const historial         = require('./auth.historial');
const fs                = require('fs');
const folderHistorial   = './public/historialPdf/';
const _                 = require('lodash');
const User              = require('./auth.dao');

exports.createAppointments = async (req, res) => {
    const newAppointment = {
      id:               req.body.id,
      name:             req.body.name,
      date:             Date.now(),
      dateA:            req.body.dateA,
      typeAppointment:  req.body.typeAppointment,
      attended:         'No'
    };
    const date      = new Date(newAppointment.dateA).toLocaleTimeString()
    const dateSplit = date.split(':');
    const hourDate  = parseInt(dateSplit[0])
    if(hourDate > 7 && hourDate < 19){
      const minuteDate = (Math.sqrt(parseInt(dateSplit[1])) - 5);

    }else{
  
    }
    const preventAppointmentPreviu = await preventAppointment(
      newAppointment.id, newAppointment.name, newAppointment.dateA
    );
    if (preventAppointmentPreviu) {
      res.status(409).send({
        status: 'info',
        message: preventAppointmentPreviu,
      });
      return false;
    }
    await handleCreateAppointment(newAppointment);
    res.status(200).send({
      status: 'success',
      message: 'Cita creada con exito'
    });
    return;
  };
  
  exports.showAppointments = async (req, res) => {
    const id = parseInt(req.params.id);
    if (!req) {
      res.status(500).send({
        status:   'error',
        message:  'Error en el envio de datos al servidor',
      });
      return;
    }
    if (req.params.name) {
      const name = req.params.name;
      const Appointments = await Appointment.find({ id: id, name: name });
      let dateAppointments = [];
      if (_.size(Appointments) === 0) {
        return 'No se encontraron citas';
      }
      _.forEach(Appointments, (value, index) => {
        dateAppointments[index] = {
          id:                   value.id,
          name:                 value.name,
          dateA:                value.dateA,
          typeAppointment:      value.typeAppointment,
          attended:             value.attended,
          result:               value.result,
          medicated:            value.medicated,
          descriptionM:         value.descriptionM,
          treatment:            value.treatment,
          descriptionTreatment: value.descriptionTreatment,
          surgery:              value.surgery,
          typeSurgery:          value.typeSurgery,
          veterinary:           value.veterinary
        };
      });
      return dateAppointments;
    }
    const Appointments = await Appointment.find({ id: id });
    let dateAppointments = [];
    if (_.size(Appointments) === 0) {
      res.send({
        status:   'info',
        message:  'No se encontraron citas',
      });
      return;
    }
    _.forEach(Appointments, (value, index) => {
      dateAppointments[index] = {
        id:                     value.id,
        name:                   value.name,
        dateA:                  value.dateA,
        typeAppointment:        value.typeAppointment,
        attended:               value.attended,
        result:                 value.result,
        medicated:              value.medicated,
        descriptionM:           value.descriptionM,
        treatment:              value.treatment,
        descriptionTreatment:   value.descriptionTreatment,
        surgery:                value.surgery,
        typeSurgery:            value.typeSurgery
      };
    });
    await res.send({ dateAppointments });
    return;
  };
  
  exports.download = async (req, res) => {
    try {
      const { id, name } = req.params;
      if (fs.existsSync(folderHistorial + id + name)) {
        fs.unlink(folderHistorial + nameFile, (exist) => {
        });
      }
      const listAppointments  = await this.showAppointments(req);
      const petDescription    = await this.showPetsAppointmentsDoctor({ id, name });
      const informationPerson = await dataPerson(id)
      const nameFile          = await historial(listAppointments, ...petDescription, informationPerson)
      res.download(folderHistorial + nameFile)
      setTimeout(() => {
        fs.unlink(folderHistorial + nameFile, (exist) => {})
      }, 1000);
    } catch (error) {
        res.status(404).send({
          status:   'info',
          message:  'No hay historial para esta mascota',
        });
      return;
    }
  };
  
  /* ADMINISTRADOR */
  
  exports.showPetsAppointmentsDoctor = async (req, res) => {
    let petsQuery = [];
    let dataPet   = [];
    if (!req) {
      res.status(500).send({
        status: 'error',
        message: 'Error en el envio de datos al servidor',
      });
      return;
    }
    if (req.body) {
      const datesPets = [...req.body];
      for (let index = 0; index < datesPets.length; index++) {
        petsQuery[index] = await Pet.find({
          id: datesPets[index]['id'],
          name: datesPets[index]['name'],
        });
        _.forEach(petsQuery[index], (value) => {
          dataPet[index] = {
            id:           parseInt(value.id),
            name:         value.name,
            race:         value.race,
            species:      value.species,
            gender:       value.gender,
            date_born:    value.date_born,
            vaccinesO:    value.vaccinesO,
            vaccines:     value.vaccines,
            imageUrl:     value.imageUrl
          };
        });
      }
      if (_.isEmpty(dataPet)) {
        res.status(401).send({
          status:   'error',
          message:  'No se encontraron datos de las mascotas',
        });
        return;
      } else {
        res.send({ dataPet });
        return;
      }
    } else {
      const petQuery = await Pet.find({
        id:   req.id,
        name: req.name,
      });
      return petQuery;
    }
  };
  
  exports.showAppointmentsDoctor = async (req, res) => {
    const AppointmentsDoctor = await Appointment.find({ attended: 'NO' });
    let Appointments = [];
    _.forEach(AppointmentsDoctor, (value, index) => {
        Appointments[index] = {
            id:               value.id,
            name:             value.name,
            dateA:            value.dateA,
            typeAppointment:  value.typeAppointment,
        };
    });
    const dateAppointments = _.orderBy( Appointments, ['dateA'],['asc', 'desc'] );
    if (Object.keys(dateAppointments).length === 0)
    return res.send(dateAppointments);
    await res.send({ dateAppointments });
    return;
  };
  
  exports.showPetsDoctor = async (req, res) => {
    if (!req) return res.status(500).send('Server error');
    const { namePet } = req.body;
    let namePets = ''
    namePet ? namePets = namePet.toUpperCase() : namePets = ''
    const dataPet     = await Pet.find({ name: { $regex: `^${namePets}` } });
    if (_.isEmpty(dataPet)) {
      res.status(404).send({
        status: 'error',
        message: 'No existe ninguna mascota con ese nombre',
      });
      return;
    }
    await res.send({ dataPet });
    return;
  };
  
  exports.updateAppointmentsDoctor = async (req, res) => {
    if (!req) return res.status(500).send('Server error');
    const newAppointment = {...req.body}
    await Appointment.findOneAndUpdate(
      { id: newAppointment.id, name: newAppointment.name, dateA: newAppointment.dateA },
      {
        $set: {
          attended:                 newAppointment.attended,
          result:                   newAppointment.result,
          medicated:                newAppointment.medicated,
          descriptionM:             newAppointment.descriptionM,
          treatment:                newAppointment.treatment,
          descriptionTreatment:     newAppointment.descriptionTreatment,
          surgery:                  newAppointment.surgery,
          typeSurgery:              newAppointment.typeSurgery,
          veterinary:               newAppointment.veterinary
        },
      },
      async (err, appoinment) => {
        if (err) {
          res.status(500).send({
            status:   'error',
            message:  'Error en el envio de datos al servidor',
          });
          return;
        }
        if (!appoinment) {
          res.status(409).send({
            status:   'error',
            message:  'La cita no existe',
          });
          return;
        } else {
          res.send({
            status:   'success',
            message:  'Cita atendida con exito',
          });
          return;
        }
      }
    );
    return;
  };
  
  exports.assentAppointmentDoctor = async (req, res) => {
    if (!req) {
      res.status(500).send({
        status:   'error',
        message:  'Ops ... algo paso al enviar los datos',
      });
      return;
    }
    const { id, name, dateA } = req.body;
    Appointment.findOneAndUpdate(
      { id, name, dateA },
      {
        $set: {
          attended: 'SI',
          result: 'No se presento a la cita',
        },
      },
      async (error, appoinment) => {
        if (error) {
          res.status(404).send({
            status:   'error',
            message:  'No se pudo dar de baja a la cita',
          });
          return;
        }
        res.send({
          status:   'success',
          message:  'Cita dada de baja con exito',
        });
        return;
      }
    );
  };

const handleCreateAppointment = async (newAppointment) => {
    let appoinmentUser = {}
    Appointment.createAppintment(newAppointment, async (err, appoinments) => {
        if (err) {
            return (error = 'Error en el envio de datos al servidor')
        } else {
            appoinmentUser = {
                id:               appoinments.id,
                name:             appoinments.name,
                date:             appoinments.dateA,
                typeAppointment:  appoinments.typeAppointment
            };
        }
    });
    return appoinmentUser;
}
  
const preventAppointment = async (id, name, dateA) => {
    try {
        const searchAppointment = await Appointment.findOne({
            id,
            name,
            attended: 'NO'
        });
        if(searchAppointment) { 
            return (error = `Ya existe una cita previa para ${searchAppointment.name}`); 
        }
        const searchAppointmentDate = await Appointment.findOne({ dateA });
        if(searchAppointmentDate){
            return (error = `Ya existe una cita previa para esa fecha`); 
        }
    } catch (err) {
        return (error = 'Ocurrio un error inexperado');
    }
}

const dataPerson = async (id) => {
  const user = await User.findOne({id})
  return user;
}