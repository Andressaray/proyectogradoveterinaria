const pdf               = require('html-pdf');
const folderPdf         = './public/historialPdf/';
const options = {
  format: "Letter",
  orientation: "landscape",
  border: {
    top:    "0.5in", // default is 0, units: mm, cm, in, px
    right:  "0.5in",
    bottom: "0.5in",
    left:   "0.5in",
  }
};
async function historialPet(listAppointments, dataPet, dataPerson) {
  const { name, race, species, gender, date_born } = dataPet;
  return new Promise((resolve, reject) => {
    const bgc = '#F1F1F1';
    let styleTable = 'width: 100%; text-align:center; border-collapse: collapse;';
    let styleTitle = 'text-align:center;';
    let nameFile = '';
    let contentMedicated = '';
    let contentAppointment = '';
    contentAppointment = `
            <style>
                span{
                    font-family: cursive;
                    font-size: 10px;
                }
            </style>   
            <table border='1' style='${styleTable}'>
                <thead>
                    <tr>
                        <th style='background-color: ${bgc};'><b>VETERINARIA DOCTOR VACCA<b></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Dirección: Calle 10 # 41 - 39 - Celular: 3112944614</td>
                    </tr>
                    <tr>
                        <td><span><i>Nuestra prioridad es la salud y el bienestar de tu mascota</i></span></td>
                    </tr>
                </tbody>
            </table>
            <br>
            <table border='1' style='${styleTable}'>
                <thead>
                    <tr>
                        <th colspan="5">Datos del dueño</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style='background-color: ${bgc};'><b>Cedula</b></td>
                        <td style='background-color: ${bgc};'><b>Nombre</b></td>
                        <td style='background-color: ${bgc};'><b>Apellido</b></td>
                        <td style='background-color: ${bgc};'><b>Correo</b></td>
                        <td style='background-color: ${bgc};'><b>Celular</b></td>
                    </tr>
                    <tr>
                        <td>${dataPerson.id}</td>
                        <td>${dataPerson.name}</td>
                        <td>${dataPerson.lastname}</td>
                        <td>${dataPerson.email}</td>
                        <td>${dataPerson.phone}</td>
                    </tr>
                </tbody>
            </table>
            <br>
            <table border='1' style='${styleTable}'>
                <thead>
                    <tr>
                        <th colspan="5">Datos de la mascota</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style='background-color: ${bgc};'><b>Nombre</b></td>
                        <td style='background-color: ${bgc};'><b>Raza</b></td>
                        <td style='background-color: ${bgc};'><b>Especie</b></td>
                        <td style='background-color: ${bgc};'><b>Genero</b></td>
                        <td style='background-color: ${bgc};'><b>Fecha de Nacimiento</b></td>
                    </tr>
                    <tr>
                        <td>${name}</td>
                        <td>${race}</td>
                        <td>${species}</td>
                        <td>${gender}</td>
                        <td>${date_born.toLocaleDateString()}</td>
                    </tr>
                </tbody>
            </table>
            <h5 style='${styleTitle}'>CITAS</h5>
            <table border='1' style='${styleTable}'>
                <thead style='background-color: ${bgc};'>
                    <tr>
                        <th>T. de cita</th>
                        <th colspan='2'>Fecha</th>
                        <th>Atendio</th>
                        <th colspan='5'>Resultado</th>
                    </tr>
                </thead>
                <tbody>`;

        contentMedicated = `
            <table border='1' style='${styleTable}'>
                <thead style='background-color: ${bgc};'>
                    <th>Medicado</th>
                    <th>T. Medicado</th>
                    <th>Tratamiento</th>
                    <th>D. Tratamiento</th>
                    <th>Sugerencia</th>
                    <th>T. Sugerencia</th>
                </thead>
                <tbody>`;

        listAppointments.forEach((appointment) => {
            const fecha = new Date(appointment.dateA).toLocaleDateString();
            const hora  = new Date(appointment.dateA).toLocaleTimeString('en-CO');
            nameFile    = appointment.id + appointment.name;
            contentAppointment += `
                <tr>
                    <td>${appointment.typeAppointment}</td>
                    <td colspan='2'>${fecha} - ${hora}</td>
                    <td>${appointment.veterinary}</td>
                    <td colspan='5'>${appointment.result ? appointment.result : ''}</td>
                </tr>`;
            contentMedicated += `
                <tr>
                    <td>${appointment.medicated ? appointment.medicated : ''}</td>
                    <td>${appointment.treatment ? appointment.treatment : ''}</td>
                    <td>${appointment.descriptionTreatment ? appointment.descriptionTreatment : ''}</td>
                    <td>${appointment.descriptionM ? appointment.descriptionM : ''}</td>
                    <td>${appointment.surgery ? appointment.surgery : ''}</td>
                    <td>${appointment.typeSurgery ? appointment.typeSurgery : ''}</td>
                </tr>
            `;
        });
    contentAppointment+= `</tbody></table><br>`;
    contentMedicated+= `</tbody></table><br>`;
    const content = contentAppointment + contentMedicated;
    pdf
      .create(content, options)
      .toFile(`${folderPdf + nameFile}.pdf`, (err, res) => {
        if (err) {
          reject(err);
        } else {
          resolve(`${nameFile}.pdf`);
        }
      });
  });
}
module.exports = historialPet;
