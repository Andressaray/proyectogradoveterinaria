const Opinion   = require('./auth.dao.opinions');
const moment    = require('moment');
const _         = require('lodash');

exports.opinionCreateUsers = async (req, res) => {
    const opinionCreate = {
      id:             req.body.id,
      name:           req.body.name,
      calification:   req.body.calification,
      description:    req.body.description,
      time:           Date.now()
    };  
    const { profile } = req.body
    if (!req) {
      res.status(500).send({
        status: 500,
        message: 'Error los datos no fueron obtenidos',
      });
      return;
    }
  
    if(!profile){
      const opinionOp = await getOpinions(opinionCreate.id);
      if (!opinionOp.status){
        res.status(404).send({
          status: 'info',
          message: opinionOp.message
        })
        return
      }
    }
  
    Opinion.create(opinionCreate, async (err, opinion) => {
      if (err) {
        await res.status(503).send({
          status:   503,
          message:  'Ya has hecho un comentario antes'
        });
        return;
      }
       else {
        await res.send({
          status:   'success',
          message:  'Opinion guardada con exito'
        });
        return;
      }
    });
  };
  
  exports.showOpinion = async (req, res) => {
    const opinions = Opinion.find({}, async (err, opinion) => {
      if (_.isEmpty(opinions)) {
          res.status(404).send({
            status:   'info',
            message:  'No existen opiniones'
          });
        } else {
          await res.send(opinion);
        }
    }).sort({$natural: -1}).limit(5)
  
  };
  
  exports.deleteOpinion = async (req, res) => {
    const { id_ } = req.body
    try {
      await Opinion.findByIdAndRemove(id_, async (err, opinion) => {
        if(err) {
          res.status(404).send({
            status:   'error',
            message:  'No se pudo borrar el comentario'
          });
          return
        }else{
          res.status(200).send({
            status: 'success',
            message: 'Comentario borrado con exito'
          })
          return
        }
      })
    } catch (error) {
        throw error = 'Error, no se borrar el comentario'
    }
  };
  
  exports.updateOpinion= async (req, res) => {
    const responseOpionion = {...req.body}
    Opinion.findOneAndUpdate(
      { id_: responseOpionion.id},
        {
        $set: {
            veterinary:         responseOpionion.veterinary,
            messageResponse:    responseOpionion.messageResponse,
            timeResponse:       responseOpionion.timeResponse
          },
      },
      async (err, opinion) => {
        if (err) {
          res.status(500).send({
            status:   'error',
            message:  'Erro en el envio de datos al servidor',
          });
          return;
        }
        if (!opinion) {
          res.status(409).send({
            status:   'error',
            message:  'El comentario no exite',
          });
          return;
        } else {
          res.send({
            status:   'success',
            message:  'Comentario respondido con exito',
          });
          return;
        }
      }
    );
  }
  
  const getOpinions = async (id) => {
    let message     = { status: true }
    const timeNow   = moment(new Date())
    let daysOpinion = 0
    let timeConvert = ''
    const allOpinions = await Opinion.find({ id })
    allOpinions.forEach(({time}) => {
        timeConvert = moment(time)
        daysOpinion = timeNow.diff(timeConvert, 'days');
        if(daysOpinion < 15) {
          let days = 15 - daysOpinion;
          message.status = false
          message.message = `Podras hacer otro comentario dentro de ${days} días`
          return message
        }
    })
    return message
  }